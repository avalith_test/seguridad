# Avalith Test

### RUN

1. Create a database with dbavalith name on Mysql and set user and password in application.properties file.

```
CREATE SCHEMA dbavalith DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
```

2. Run Unit Test InitialTest.Java 

```
 mvn -Dtest=InitialTest test
```

3. Run Project 

```
  mvn spring-boot:run
```

4. Use POSTMAN resources with Authorization Basic
```
 user: admin@hotmail.com
 password: admin
```