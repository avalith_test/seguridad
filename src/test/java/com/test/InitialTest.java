package com.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.test.enums.Response;
import com.test.enums.Roles;
import com.test.model.Rol;
import com.test.model.User;
import com.test.service.RolService;
import com.test.service.UserService;

@SpringBootTest
public class InitialTest {
	@Autowired
	private RolService service;

	@Autowired
	private UserService serviceUser;

	@Test
	void createInitData() {

		/* Create initial role and set to current user the role */
		Response status = service.create(new Rol(Roles.Admin.name()));
		assertNotNull(status);
		if (status.equals(Response.success)) {
			Rol roladmin = (Rol) status.getCustomObject();
			/* Create initial user */
			User user = new User("admin@hotmail.com", "admin");
			status = serviceUser.create(user);
			assertTrue(status.equals(Response.success) || status.equals(Response.error002));
			if (status.equals(Response.success)) {
				user = (User) status.getCustomObject();
				user.setRolCollection(new ArrayList<Rol>());
				user.getRolCollection().add(roladmin);
				User addrol = serviceUser.repo.save(user);
				assertTrue(addrol.getUsemail().equals(user.getUsemail()));
			}
		} 
		/* Create Employee Role */
		status = service.create(new Rol(Roles.Employee.name()));
		assertNotNull(status);

	}

}
