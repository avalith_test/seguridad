package com.test.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Repository
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "useid", length = 50)
	private Integer useid;
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "usepassword", nullable = false)
	private String usepassword;
	@Column(name = "usecorreo", unique = true, length = 50, nullable = false)
	private String usemail;

	@JsonIgnore
	@JoinTable(name = "user_rol", joinColumns = {
			@JoinColumn(name = "useid", referencedColumnName = "useid") }, inverseJoinColumns = {
					@JoinColumn(name = "rolid", referencedColumnName = "rolid") })
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Collection<Rol> rolCollection;

	public User() {

	}

	public User(String usemail, String usepassword) {
		this.usepassword = usepassword;
		this.usemail = usemail;
	}

	public Integer getUseid() {
		return useid;
	}

	public void setUseid(Integer useid) {
		this.useid = useid;
	}

	public String getUsepassword() {
		return usepassword;
	}

	public void setUsepassword(String usepassword) {
		this.usepassword = usepassword;
	}

	public String getUsemail() {
		return usemail;
	}

	public void setUsemail(String usemail) {
		this.usemail = usemail;
	}

	public Collection<Rol> getRolCollection() {
		return rolCollection;
	}

	public void setRolCollection(Collection<Rol> rolCollection) {
		this.rolCollection = rolCollection;
	}

}
