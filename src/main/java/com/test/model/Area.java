package com.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.stereotype.Repository;

@Entity
@Repository
public class Area {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "areid", length = 50)
	private Integer areid;
	@Column(name = "arename", nullable = false)
	private String arename;

	public Area() {

	}

	public Area(Integer areid) {
		this.areid = areid;
	}

	public Integer getAreid() {
		return areid;
	}

	public void setAreid(Integer areid) {
		this.areid = areid;
	}

	public String getArename() {
		return arename;
	}

	public void setArename(String arename) {
		this.arename = arename;
	}

}
