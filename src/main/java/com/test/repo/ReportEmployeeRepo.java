package com.test.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.test.model.view.ViewEmployeeVote;

@Repository
public interface ReportEmployeeRepo extends JpaRepository<ViewEmployeeVote, Integer> {

	@Query(value = "SELECT v.empidvoted as empid, e.empname, e.emplastname, a.arename, v.votdate, count(*) as votes "
			+ " FROM vote as v " + "JOIN employee as e on v.empidvoted=e.empid "
			+ " JOIN area as a on a.areid=e.areid WHERE year(v.votdate) = year(:date) and  month(v.votdate) = month(:date) "
			+ " GROUP BY v.empidvoted, e.empname, e.emplastname, a.arename order by votes DESC limit 10", nativeQuery = true)
	List<ViewEmployeeVote> findMoreVoteEmployeeByMonth(@Param("date") Date date);

	@Query(value = "SELECT v.empidvoted as empid, e.empname, e.emplastname, a.arename, v.votdate, count(*) as votes "
			+ " FROM vote as v " + "JOIN employee as e on v.empidvoted=e.empid "
			+ " JOIN area as a on a.areid=e.areid WHERE v.areid=:areid"
			+ " GROUP BY v.empidvoted, e.empname, e.emplastname, a.arename order by votes desc limit 10", nativeQuery = true)
	List<ViewEmployeeVote> findMoreVoteEmployeeByArea(@Param("areid") Integer areid);

	@Query("SELECT COUNT(u) FROM Employee u")
	Long countEmployees();
}
