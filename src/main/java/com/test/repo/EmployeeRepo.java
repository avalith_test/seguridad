package com.test.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.test.model.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {

	Employee findByEmpidentification(String empidentification);

	@Query(value = "SELECT e.* FROM employee as e WHERE e.areid = ?1 AND e.empid = ?2", nativeQuery = true)
	Employee findByArea(Integer areid, Integer employeeid);
}
