package com.test.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.enums.Response;
import com.test.model.view.ViewEmployeeVote;
import com.test.service.ReportEmployeeService;

@RestController
@RequestMapping("/reportemployee")
public class ReportEmployeeController {

	@Autowired
	private ReportEmployeeService service;

	/**
	 * Get a list votes by date limit 10
	 * 
	 * @return
	 */
	@GetMapping
	@RequestMapping("/morevotesbydate")
	public List<ViewEmployeeVote> morevotesbydate(@RequestBody ViewEmployeeVote employee,
			HttpServletResponse response) {
		Response status = Response.success;
		if (employee == null || employee.getVotdate() == null) {
			status = Response.error008;
		}
		if (status.equals(Response.success)) {
			return service.repo.findMoreVoteEmployeeByMonth(employee.getVotdate());
		}
		response.setStatus(status.getStatus().value());
		response.setHeader("message", status.getMensaje());
		return new ArrayList<>();
	}

	/**
	 * Get a list votes by area limit 10
	 * 
	 * @return
	 */
	@GetMapping(value = "/morevotesbyarea/{id}")
	public List<ViewEmployeeVote> morevotesbyarea(@PathVariable("id") Integer id) {
		return service.repo.findMoreVoteEmployeeByArea(id);
	}

	/**
	 * Get a list votes by area limit 10
	 * 
	 * @return
	 */
	@GetMapping
	@RequestMapping("/countemployees")
	public Long countemployees() {
		return service.repo.countEmployees();
	}

}
