package com.test.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.test.enums.Response;
import com.test.model.Rol;
import com.test.model.User;
import com.test.repo.RolRepo;
import com.test.repo.UserRepo;

/**
 * Any logical business over entity user is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class UserService implements UserDetailsService {
	@Autowired
	public UserRepo repo;
	@Autowired
	public RolRepo repoRol;

	@Override
	public UserDetails loadUserByUsername(String usermail) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = repo.findByUsemail((usermail));
		if (user != null) {
			List<Rol> roles = repoRol.findByUserId(user.getUseid());
			List<GrantedAuthority> rolesSecurity = new ArrayList<>();
			for (Rol rol : roles) {
				rolesSecurity.add(new SimpleGrantedAuthority(rol.getRolname()));
			}
			return new org.springframework.security.core.userdetails.User(user.getUsemail(), user.getUsepassword(),
					rolesSecurity);
		} else {
			return null;
		}

	}

	public Response create(User objeto) {
		Response validate = validate(objeto, true);
		if (validate.equals(Response.success)) {
			objeto = this.repo.save(objeto);
			if (objeto != null) {
				Response status = Response.success;
				status.setCustomObject(objeto);
				return status;
			} else {
				return Response.error001;
			}
		} else {
			return validate;
		}

	}

	public Response validate(User objeto, Boolean newObject) {

		if (objeto == null) {
			return Response.error001;
		}
		if (!newObject && objeto.getUseid() == null)
			return Response.error001;

		if (objeto.getUsemail() == null || objeto.getUsepassword() == null || objeto.getUsemail().isEmpty()
				|| objeto.getUsepassword().isEmpty())
			return Response.error001;

		/* Validate User Mail */
		User objetoUsuario = this.repo.findByUsemail(objeto.getUsemail());

		/* if the current mail exist when is a new user */
		if (objetoUsuario != null && newObject)
			return Response.error002;

		/* If you are updating and other user get this mail */
		if (!newObject && objetoUsuario != null && objetoUsuario.getUseid().equals(objeto.getUseid()))
			return Response.error002;

		/* Encrypt password */
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		if (newObject) {
			objeto.setUsepassword(bcrypt.encode(objeto.getUsepassword()));
		} else if (!objetoUsuario.getUsepassword().equals(objeto.getUsepassword())) {
			objeto.setUsepassword(bcrypt.encode(objeto.getUsepassword()));
		}

		return Response.success;
	}

}
