package com.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.repo.ReportEmployeeRepo;

/**
 * Any logical business over entity Area is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class ReportEmployeeService {
	@Autowired
	public ReportEmployeeRepo repo;

}
