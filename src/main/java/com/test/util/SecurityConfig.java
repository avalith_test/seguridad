package com.test.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.test.enums.Roles;
import com.test.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService service;

	@Autowired
	private BCryptPasswordEncoder bcrypt;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		return bcrypt;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/* data base validate user */
		auth.userDetailsService(service).passwordEncoder(bcrypt);

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers("/rol").hasAuthority(Roles.Admin.name())
		.antMatchers("/user").hasAuthority(Roles.Admin.name())
		.antMatchers("/area").hasAuthority(Roles.Admin.name())
		.antMatchers("/employee").hasAuthority(Roles.Admin.name())
		.antMatchers("/reportemployee/**").hasAuthority(Roles.Admin.name())
		.antMatchers("/vote").hasAnyAuthority(Roles.Employee.name(), Roles.Admin.name())
		.anyRequest().fullyAuthenticated()
		.and().httpBasic();
	}
}
